use rand::prelude::*;

use itertools::Itertools;
use std::collections::HashMap;

use tinyvec::{array_vec, ArrayVec};

const MAX_SMALL: usize = 12;
type Word = ArrayVec<[u8; MAX_SMALL]>;
type Matches = ArrayVec<[Match; MAX_SMALL]>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Match {
    Exact { l: u8, idx: u8 },
    Inexact { l: u8, idx: u8 },
}

impl Default for Match {
    fn default() -> Self {
        Match::Exact { l: 0, idx: u8::MAX }
    }
}

fn matches(guess: &Word, word: &Word) -> Matches {
    let mut ms = Matches::new();
    let mut is_used = array_vec!([bool; MAX_SMALL]);
    for _ in word {
        is_used.push(false);
    }
    for (i, l) in guess.iter().cloned().enumerate() {
        if word[i] == l {
            ms.push(Match::Exact { l, idx: i as u8 });
            is_used[i] = true;
        }
    }
    for (i, l) in guess.iter().cloned().enumerate() {
        if let Some(idx) = word.iter().position(|w| *w == l) {
            if !is_used[idx] && word[i] != l {
                ms.push(Match::Inexact { l, idx: i as u8 });
                is_used[idx] = true;
            }
        }
    }
    ms
}

fn word_list(len: usize) -> Vec<Word> {
    let word_file = include_str!("words.txt");
    word_file
        .split_whitespace()
        .filter_map(|w| {
            if w.len() == len {
                Some(Word::from_iter(w.as_bytes().iter().cloned()))
            } else {
                None
            }
        })
        .collect()
}

fn word5_list() -> Vec<Word> {
    let word_file = include_str!("words5.txt");
    word_file
        .split_whitespace()
        .map(|w| Word::from_iter(w.as_bytes().iter().cloned()))
        .collect()
}

#[allow(unused)]
fn riddler_split(word_list: &[Word], guess_list: &[Word]) -> (Word, HashMap<Matches, Vec<Word>>) {
    // Split by finding the most sub clusters.
    // Seems worse than squared sub sizes for making greedy choices.
    let mut pivot = word_list[0].clone();
    let mut pivot_score = 0;
    let mut pivot_match_map = HashMap::new();

    for guess in guess_list {
        let mut match_map = HashMap::<Matches, Vec<Word>>::new();
        for word in word_list {
            let mut ms = matches(guess, word);
            ms.sort_unstable();
            let ws = match_map.entry(ms).or_default();
            ws.push(*word);
        }
        let guess_score = match_map.len();
        if guess_score > pivot_score {
            pivot = *guess;
            pivot_score = guess_score;
            pivot_match_map = match_map;
        }
    }
    // eprint_match_map(&pivot, &pivot_match_map);
    (pivot, pivot_match_map)
}

#[allow(unused)]
fn split_list(word_list: &[Word], guess_list: &[Word]) -> (Word, HashMap<Matches, Vec<Word>>) {
    let mut pivot = word_list[0].clone();
    let mut pivot_loss = word_list.len().pow(2) + 1;
    let mut pivot_match_map = HashMap::new();

    for guess in guess_list {
        let mut match_map = HashMap::<Matches, Vec<Word>>::new();
        for word in word_list {
            let mut ms = matches(guess, word);
            ms.sort_unstable();
            let ws = match_map.entry(ms).or_default();
            ws.push(*word);
        }
        let guess_loss = match_map
            .values()
            .map(|ws| {
                if ws.len() == 1 && ws[0] == *guess {
                    0
                } else {
                    ws.len().pow(2)
                }
            })
            .sum::<usize>();
        if guess_loss < pivot_loss {
            pivot = *guess;
            pivot_loss = guess_loss;
            pivot_match_map = match_map;
        }
    }
    // eprint_match_map(&pivot, &pivot_match_map);
    (pivot, pivot_match_map)
}

fn fmt_matches(ms: &Matches, len: usize) -> String {
    let mut fmt = vec![b'_'; len];
    for m in ms {
        match *m {
            Match::Exact { idx, .. } => fmt[idx as usize] = b'X',
            Match::Inexact { idx, .. } => fmt[idx as usize] = b'+',
        }
    }
    String::from_utf8(fmt).unwrap()
}

fn fmt_word(w: &Word) -> String {
    String::from_utf8(w.to_vec()).unwrap()
}

#[allow(unused)]
fn eprint_match_map(guess: &Word, mm: &HashMap<Matches, Vec<Word>>) {
    eprintln!("{}", fmt_word(guess));
    for (m, ws) in mm {
        eprintln!("{}:", fmt_matches(m, guess.len()));
        for w in ws {
            eprintln!("  {}", fmt_word(w));
        }
    }
}

fn opt_tree(word_list: &[Word], guess_list: &[Word]) -> (usize, WordleTree) {
    if word_list.len() == 1 {
        return (1, WordleTree::Leaf { word: word_list[0] });
    }
    let mut best_tree = WordleTree::Leaf {
        word: guess_list[0],
    };
    let mut best_tree_size = word_list.len() + 999;
    for guess in guess_list {
        let mut tree_size = 0;
        let mut branches = HashMap::new();
        let (pivot, match_map) = split_list(word_list, &[guess.clone()]);
        assert_eq!(&pivot, guess, "{} != {}", fmt_word(&pivot), fmt_word(guess));
        // eprint_match_map(&pivot, &match_map);
        if match_map.len() == 1 {
            continue;
        }
        for (m, sub_list) in match_map
            .into_iter()
            .sorted_unstable_by_key(|(_m, wl)| wl.len())
            .rev()
        {
            assert_ne!(sub_list.len(), word_list.len());
            let (sub_size, sub_tree) = if sub_list == [guess.clone()] {
                (
                    0,
                    WordleTree::Leaf {
                        word: guess.clone(),
                    },
                )
            } else {
                opt_tree(&sub_list, guess_list)
            };
            tree_size = tree_size.max(sub_size);
            branches.insert(m, Box::new(sub_tree));
            if tree_size >= best_tree_size {
                break;
            }
        }
        if tree_size < best_tree_size {
            best_tree_size = tree_size;
            best_tree = WordleTree::Node {
                pivot: guess.clone(),
                branches,
                size: tree_size + 1,
            };
        }
    }
    (best_tree_size + 1, best_tree)
}

enum WordleTree {
    Node {
        pivot: Word,
        size: usize,
        branches: HashMap<Matches, Box<WordleTree>>,
    },
    Leaf {
        word: Word,
    },
}

impl WordleTree {
    fn from_word_list(word_list: &[Word], guess_list: &[Word]) -> Self {
        if word_list.len() == 1 {
            return WordleTree::Leaf { word: word_list[0] };
        }
        let (pivot, match_map) = split_list(word_list, guess_list);
        // let (pivot, match_map) = riddler_split(word_list, guess_list);
        let branches = match_map
            .into_iter()
            .map(|(m, word_list)| {
                (
                    m,
                    Box::new(WordleTree::from_word_list(&word_list, guess_list)),
                )
            })
            .collect();
        WordleTree::Node {
            pivot,
            size: word_list.len(),
            branches,
        }
    }

    fn size(&self) -> usize {
        match self {
            &WordleTree::Leaf { .. } => 1,
            &WordleTree::Node { size, .. } => size,
        }
    }

    fn is_leaf(&self) -> bool {
        match self {
            WordleTree::Leaf { .. } => true,
            _ => false,
        }
    }

    fn pretty_print(&self, prefix: &str) {
        match self {
            WordleTree::Leaf { word } => {
                println!("{}{}", prefix, std::str::from_utf8(word).unwrap())
            }
            WordleTree::Node {
                pivot, branches, ..
            } => {
                let branches = branches
                    .iter()
                    .sorted_by_key(|(m, wt)| (wt.size(), *m))
                    .collect_vec();
                println!(
                    "{}{}: {}",
                    prefix,
                    std::str::from_utf8(&pivot).unwrap(),
                    // branches[branches.len() - 1].1.size()
                    self.size()
                );
                for (m, wt) in branches {
                    print!("{}  {}:", prefix, fmt_matches(m, pivot.len()));
                    if wt.is_leaf() {
                        wt.pretty_print(" ");
                    } else {
                        println!(" {}", wt.size());
                        wt.pretty_print(&format!("{}    ", prefix));
                    }
                }
            }
        }
    }

    fn probability(&self, num_guesses: usize) -> f64 {
        if num_guesses == 0 {
            return 0.0;
        }
        match self {
            WordleTree::Leaf { .. } => 1.0,
            WordleTree::Node {
                ref pivot,
                ref branches,
                ..
            } => {
                let mut prob = 0.0;
                for sub_tree in branches.values() {
                    if let WordleTree::Leaf { word } = **sub_tree {
                        if word == *pivot {
                            prob += sub_tree.size() as f64 / self.size() as f64
                                * sub_tree.probability(num_guesses)
                        } else {
                            prob += sub_tree.size() as f64 / self.size() as f64
                                * sub_tree.probability(num_guesses - 1)
                        }
                    } else {
                        prob += sub_tree.size() as f64 / self.size() as f64
                            * sub_tree.probability(num_guesses - 1)
                    }
                }
                prob
            }
        }
    }
}

fn main() {
    let n = std::env::args()
        .nth(1)
        .unwrap_or("5".to_string())
        .parse()
        .unwrap();
    let mut gs = word_list(n);
    let mut ws = if n == 5 { word5_list() } else { gs.clone() };
    if n == 4 {
        // 4 20 3 leads to an example where we can do better than the greedy solution.
        let count = std::env::args()
            .nth(2)
            .map(|s| s.parse().unwrap())
            .unwrap_or(10);
        let seed = std::env::args()
            .nth(3)
            .map(|s| s.parse().unwrap())
            .unwrap_or(4);
        gs.shuffle(&mut rand_pcg::Pcg64::seed_from_u64(seed));
        ws.shuffle(&mut rand_pcg::Pcg64::seed_from_u64(seed));
        gs.truncate(count);
        ws.truncate(count);
        let (_size, tree) = opt_tree(&ws, &gs);
        tree.pretty_print("");
    }
    let tree = WordleTree::from_word_list(&ws, &gs);
    tree.pretty_print("");
    for num_guesses in 1..=6 {
        println!("{}: {:7.3}%", num_guesses, 100.0 * tree.probability(num_guesses));
    }
}
